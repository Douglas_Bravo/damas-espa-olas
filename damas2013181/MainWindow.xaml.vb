﻿Class MainWindow 
    Dim a As Integer = 1
    Dim x As Double = 0
    Dim arregloR(11) As UserControl
    Dim arregloN(11) As UserControl
    Dim contR As Integer = 0
    Dim contN As Integer = 0
    Dim cuaB(31) As Rectangle
    Dim contB As Integer = 0
    Dim turno As Integer = 0
    Dim seleccion As Integer = 0
    Dim pieza As Integer = 0
    Dim condicion As Integer = 0
    Dim origen(1) As Integer
    Dim arregloCn(3) As Rectangle
    Dim arregloCr(3) As Rectangle
    Dim contC As Integer = 0

    '---------------------------------------------------------------------------------------------------------------------------------------
    'este metodo es para crear la estructura del cuadro de juegos  en el cual se agragara los colores tradicionales blanco y negro, tambien 
    'agregaremos de una sola vez las fichas de color rojo y negro
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub Estructura()
        Dim left As Integer = 0
        Dim top As Integer = 0

        For y As Integer = 0 To 7
            For x As Integer = 0 To 7
                Dim rect As New Rectangle
                rect.Width = 44
                rect.Height = 44

                If y Mod 2 = 0 Then
                    If x Mod 2 = 0 Then
                        rect.Fill = Brushes.White
                        cuaB(contB) = rect
                        contB += 1
                    Else
                        rect.Fill = Brushes.Black
                    End If
                Else
                    If x Mod 2 = 0 Then
                        rect.Fill = Brushes.Black
                    Else
                        rect.Fill = Brushes.White
                        cuaB(contB) = rect
                        contB += 1
                    End If
                End If
                Canvas.SetTop(rect, CDbl(top))
                Canvas.SetLeft(rect, CDbl(left))
                Tablero.Children.Add(rect)

                If rect.Fill.ToString = "#FFFFFFFF" And top >= 220 Then
                    If contR > 7 Then
                        arregloCn(contC) = rect
                        contC += 1
                    End If
                    Dim roja As New fichaRoja
                    Canvas.SetLeft(roja, CDbl(left))
                    Canvas.SetTop(roja, CDbl(top))
                    fichas.Children.Add(roja)
                    arregloR(contR) = roja
                    contR += 1
                End If
                If rect.Fill.ToString = "#FFFFFFFF" And top <= 88 Then
                    If contN < 4 Then
                        arregloCr(contN) = rect
                    End If
                    Dim negra As New fichaNegra
                    Canvas.SetLeft(negra, CDbl(left))
                    Canvas.SetTop(negra, CDbl(top))
                    fichas.Children.Add(negra)
                    arregloN(contN) = negra
                    contN += 1
                End If
                left += 44
            Next
            top += 44
            left = 0
        Next
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    'este metodo creara los eventos para que a la hora que se le de click a alguna ficha esta pueda moverse 
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub Eventos()
        For x As Integer = 0 To 11
            AddHandler arregloN(x).MouseUp, AddressOf MovimientoN
            AddHandler arregloR(x).MouseUp, AddressOf MovimientoR
        Next
        For Y As Integer = 0 To 31
            AddHandler cuaB(Y).MouseUp, AddressOf Accion
        Next
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' este metodo verificara si esta en rijo el cuadro y si no lo esta me permitira mover mi pieza a este cuadro independientemente del color
    ' de mi ficha
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub Accion(ByVal sender As Object, ByVal e As EventArgs)
        For o As Integer = 0 To 31
            If cuaB(o) Is sender Then
                If cuaB(o).Fill.ToString = "#FFFF0000" Then
                    If seleccion = 1 Then
                        Canvas.SetLeft(arregloN(pieza), Canvas.GetLeft(cuaB(o)))
                        Canvas.SetTop(arregloN(pieza), Canvas.GetTop(cuaB(o)))
                        Turnos()
                        condicion = 0
                        comer(Canvas.GetLeft(cuaB(o)), Canvas.GetTop(cuaB(o)))
                        Coronar(Canvas.GetLeft(cuaB(o)), Canvas.GetTop(cuaB(o)))
                    Else
                        Canvas.SetLeft(arregloR(pieza), Canvas.GetLeft(cuaB(o)))
                        Canvas.SetTop(arregloR(pieza), Canvas.GetTop(cuaB(o)))
                        Turnos()
                        condicion = 0
                        comer(Canvas.GetLeft(cuaB(o)), Canvas.GetTop(cuaB(o)))
                        Coronar(Canvas.GetLeft(cuaB(o)), Canvas.GetTop(cuaB(o)))
                    End If

                Else
                    MsgBox("ESTE NO ES UN MOVIMIENTO VALIDO")
                End If
            End If
        Next
        colores()
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo verificara que imagen es la que seleccionamos en este caso sera la imagen negra la que aparecera seleccionada y estara
    ' lista para ser movida
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub MovimientoN(ByVal sender As Object, ByVal e As EventArgs)
        If turno = 1 Then
            If condicion = 0 Then
                colores()
                For o As Integer = 0 To 11
                    If arregloN(o) Is sender Then
                        seleccion = 1
                        pieza = o
                        condicion = 1
                        origen(0) = Canvas.GetLeft(arregloN(o))
                        origen(1) = Canvas.GetTop(arregloN(o))
                        Saltar(Canvas.GetLeft(arregloN(o)), Canvas.GetTop(arregloN(o)), 0)
                        If sender.ToString = "WpfApplication1.coronaNegra" Then
                            MsgBox("sfdsf")
                        End If
                    End If
                Next
            Else
                MsgBox("pieza seleccionada pieza que debes jugar")
            End If
        Else
            MsgBox("espera tu turno >:-l")
        End If

    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo recibe las coordenadas de la ficha seleccionada para pintar las opciones de color rojo
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Function Coordenadas(ByVal posx As Integer, ByVal posy As Integer, ByVal y As Integer) As Boolean
        Dim colorear As Integer = 32
        For a As Integer = 0 To 31
            If Canvas.GetLeft(cuaB(a)) = posx And Canvas.GetTop(cuaB(a)) = posy Then
                colorear = a
                If seleccion = 1 Then
                    For b As Integer = 0 To 11
                        If Canvas.GetLeft(arregloR(b)) = posx And Canvas.GetTop(arregloR(b)) = posy Then
                            Return False
                        ElseIf Canvas.GetLeft(arregloN(b)) = posx And Canvas.GetTop(arregloN(b)) = posy Then
                            Return True
                        End If
                    Next
                Else
                    For b As Integer = 0 To 11
                        If Canvas.GetLeft(arregloN(b)) = posx And Canvas.GetTop(arregloN(b)) = posy Then
                            Return False
                        ElseIf Canvas.GetLeft(arregloR(b)) = posx And Canvas.GetTop(arregloR(b)) = posy Then
                            Return True
                        End If
                    Next
                End If
            End If
        Next
        If colorear <> 32 Then
            If y = 0 Then
                cuaB(colorear).Fill = Brushes.Red
                Return True
            Else
                Return True
            End If
            
        End If
        Return False
    End Function


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo me dara las opciones a donde puedo mover mi ficha y eso quiere decir que al ser la imagen clickeada esta mostrara mis
    ' opciones de movimiento en color rojo
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub colores()
        For x As Integer = 0 To 31
            cuaB(x).Fill = Brushes.White
        Next
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo verificara que imagen es la que seleccionamos en este caso sera la imagen roja la que aparecera seleccionada y estara
    ' lista para ser movida
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub MovimientoR(ByVal sender As Object, ByVal e As EventArgs)
        If turno = 2 Then
            If condicion = 0 Then
                colores()
                For o As Integer = 0 To 11
                    If arregloR(o) Is sender Then
                        seleccion = 2
                        pieza = o
                        condicion = 1
                        origen(0) = Canvas.GetLeft(arregloR(o))
                        origen(1) = Canvas.GetTop(arregloR(o))
                        Saltar(Canvas.GetLeft(arregloR(o)), Canvas.GetTop(arregloR(o)), 0)
                    End If
                Next
            Else
                MsgBox("pieza seleccionada pieza que debes jugar")
            End If
        Else
            MsgBox("Espera tu turno >:-l")
        End If     
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo me busca las coordenadas de cada ves que le doy click a una pieza para verificar si hay alguna saltar sobre ella
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub Saltar(ByVal posx As Integer, ByVal posy As Integer, ByVal x As Integer)
        If seleccion = 1 Then
            If Not Coordenadas(posx + 44, posy + 44, x) Then
                If Coordenadas(posx + 88, posy + 88, 0) Then
                    Saltar(posx + 88, posy + 88, 1)
                End If
            End If
            If Not Coordenadas(posx - 44, posy + 44, x) Then
                If Coordenadas(posx - 88, posy + 88, 0) Then
                    Saltar(posx - 88, posy + 88, 1)
                End If
            End If
        End If
        If seleccion = 2 then 
        If Not Coordenadas(posx + 44, posy - 44, x) Then
            If Coordenadas(posx + 88, posy - 88, 0) Then
                Saltar(posx + 88, posy - 88, 1)
            End If
        End If
        If Not Coordenadas(posx - 44, posy - 44, x) Then
            If Coordenadas(posx - 88, posy - 88, 0) Then
                Saltar(posx - 88, posy - 88, 1)
            End If
        End If
        End If
    End Sub

  
    '---------------------------------------------------------------------------------------------------------------------------------------
    'Este metodo verifica las coordenadas para que este pueda comer si se encuentra una pieza alrededor
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub comer(ByVal posx As Integer, ByVal posy As Integer)
        Dim y As Integer = 0
        If Buscar(posx - 88, posy - 88) Then
            eliminar(posx - 44, posy - 44)
            comer(posx - 88, posy - 88)
        ElseIf origen(0) = posx - 88 And origen(1) = posy - 88 Then
            eliminar(posx - 44, posy - 44)
        End If
        If Buscar(posx + 88, posy + 88) Then
            eliminar(posx + 44, posy + 44)
            comer(posx + 88, posy + 88)
        ElseIf origen(0) = posx + 88 And origen(1) = posy + 88 Then
            eliminar(posx + 44, posy + 44)
        End If
        If Buscar(posx + 88, posy - 88) Then
            eliminar(posx + 44, posy - 44)
            comer(posx + 88, posy - 88)
        ElseIf origen(0) = posx + 88 And origen(1) = posy - 88 Then
            eliminar(posx + 44, posy - 44)
        End If
        If Buscar(posx - 88, posy + 88) Then
            eliminar(posx - 44, posy + 44)
            comer(posx - 88, posy + 88)
        ElseIf origen(0) = posx - 88 And origen(1) = posy + 88 Then
            eliminar(posx - 44, posy + 44)
        End If
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' este metodo busca las posiciones que si estan marcadas a la hora de ya realizar el movimiento vuelva a su color norma
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Function Buscar(ByVal posx As Integer, ByVal posy As Integer) As Boolean
        For x As Integer = 0 To 31
            If Canvas.GetLeft(cuaB(x)) = posx And Canvas.GetTop(cuaB(x)) = posy Then
                If cuaB(x).Fill.ToString = "#FFFF0000" Then
                    cuaB(x).Fill = Brushes.White
                    Return True
                End If
            End If
        Next
        Return False
    End Function


    '---------------------------------------------------------------------------------------------------------------------------------------
    'este metodo eliminara la pieza del tablero , (pieza que ya haya sido comida) solamente esta desaparecera
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub eliminar(ByVal posx As Integer, ByVal posy As Integer)
        If seleccion = 1 Then
            For x As Integer = 0 To 11
                If Canvas.GetLeft(arregloR(x)) = posx And Canvas.GetTop(arregloR(x)) = posy Then
                    ' fichas.Children.Remove(arregloR(x))
                    Canvas.SetLeft(arregloR(x), 500)
                    Canvas.SetTop(arregloR(x), 500)
                End If
            Next
        Else
            For x As Integer = 0 To 11
                If Canvas.GetLeft(arregloN(x)) = posx And Canvas.GetTop(arregloN(x)) = posy Then
                    '  fichas.Children.Remove(arregloN(x))
                    Canvas.SetLeft(arregloN(x), 500)
                    Canvas.SetTop(arregloN(x), 500)
                End If
            Next
        End If
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo me premite verificar a quien de los jugadores rojo y negro le toca hacer movimiento de su pieza
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub Turnos()
        If turno = 2 Then
            turno = 1
        Else
            turno += 1
        End If
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo me permite lograr que cuando un peon esta en en las primeras casillas del lado contrario pasen a poder coronarse 
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub Coronar(ByVal posx As Integer, ByVal posy As Integer)
        If seleccion = 1 Then
            For x As Integer = 0 To 3
                If Canvas.GetLeft(arregloCn(x)) = posx And Canvas.GetTop(arregloCn(x)) = posy Then
                    MsgBox("coronado")
                    For y As Integer = 0 To 11
                        If Canvas.GetLeft(arregloN(y)) = posx And Canvas.GetTop(arregloN(y)) = posy Then
                            Dim reina As New coronaNegra
                            fichas.Children.Remove(arregloN(y))
                            arregloN(y) = reina
                            fichas.Children.Add(reina)
                            Canvas.SetLeft(reina, posx)
                            Canvas.SetTop(reina, posy)
                            AddHandler reina.MouseUp, AddressOf MovimientoN
                        End If
                    Next
                End If
            Next
        Else
            For x As Integer = 0 To 3
                If Canvas.GetLeft(arregloCr(x)) = posx And Canvas.GetTop(arregloCr(x)) = posy Then
                    MsgBox("coronado")
                    For y As Integer = 0 To 11
                        If Canvas.GetLeft(arregloR(y)) = posx And Canvas.GetTop(arregloR(y)) = posy Then
                            Dim reina As New coronaRoja
                            fichas.Children.Remove(arregloR(y))
                            arregloR(y) = reina
                            fichas.Children.Add(reina)
                            Canvas.SetLeft(reina, posx)
                            Canvas.SetTop(reina, posy)
                            AddHandler reina.MouseUp, AddressOf MovimientoR
                        End If
                    Next
                End If
            Next
        End If

    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo pondra mis cuadros en el tablero de juego y me mostrara el juego en proceso en fin esto me mostrara el tablero inicial y 
    'tambien con las piezas
    '---------------------------------------------------------------------------------------------------------------------------------------
    'Private Sub Tablero_Initialized(sender As Object, e As EventArgs) Handles Tablero.Initialized
    '  Estructura()
    ' End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    'Este metodo le dara instrucciones a mi boton en este caso es el boton iniciar, lo que hara este boton es que me mostrara mis piezas
    ' ya posicionadas para iniciar mi partida de juego
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub Jugar(ByVal sender As Object, ByVal e As EventArgs) Handles btnJugar.Click
        Estructura()
        Eventos()
        turno += 1
        condicion = 0
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo le dara intrucciones a mi boton en este caso es el boton nueva partida, lo que hara este boton es que quitara las piezas 
    'de mi tablero de juego para iniciar otra ves desde cero
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub NuevaPartida(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevaParida.Click
        Tablero.Children.Clear()
        fichas.Children.Clear()
        MsgBox("iniciando partida")
        contB = 0
        contR = 0
        contN = 0
        turno = 0
        condicion = 0
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------
    ' Este metodo le dara instrucciones a mi boton en este caso es el boton salir, lo que hara este boton es que me permitira salir por
    ' completo de la aplicacion
    '---------------------------------------------------------------------------------------------------------------------------------------
    Private Sub Salir(ByVal sender As Object, ByVal e As EventArgs) Handles btnSalir.Click
        Close()
    End Sub
End Class
